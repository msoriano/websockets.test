﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace WSClient.NF
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebSocket ws = new WebSocket("ws://127.0.0.1:7890/EchoAll"))
            {
                ws.OnMessage += Ws_OnMessage;
                ws.Connect();
                ws.Send("Hello from Client");

                Console.ReadKey();
            }
        }

        private static void Ws_OnMessage(object sender, MessageEventArgs e)
        {
            Console.WriteLine($"Receive from server: {e.Data}");

            try
            { 
            }
            catch (Exception ex)
            { 
            }
        }
    }
}
